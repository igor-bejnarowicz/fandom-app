# Fandom Recruitment App

A simple React app created by Igor Bejnarowicz in the recruitement process for Fandom.

## TL;DR

- `npm install` and
- `npm start`

should do the trick.


## Thoughts
- API calls use the cors-anywhere heroku proxy, which sometimes
(albeit in my experience quite rarely) doesn't respond which results in an error.
If that's the case, browser CORS extensions should save the day.
- Styling was not required, but some has been implemented nonetheless, 
so that the app looks somewhat decently.
- Although not mentioned in the task acceptance criterias, debounce effect for
the @mention tag's users call has been implemented, which allowed for drastic
improvement of the dropdown's readability and related user experience.

## Possible improvements
- Tests - I didn't make it in time to write any.
It doesn't really hurt (yet) with such a small codebase, but
if the application was to grow, decent test coverage would be crucial
in keeping the project reliable (and maintainable).
- Separating postsState and usersState into 2 dedicated reducers.
The singular one used currently became a bit cluttered.
