import { IPost } from '../../model/IPost';

export const RetrievePersistedPostsAction = 'RetrievePersistedPostsAction';
export const ChangeNewPostContentAction = 'ChangeNewPostContentAction';
export const AddNewPostAction = 'AddNewPostAction';

export interface IRetrievePersistedPostsAction {
  type: typeof RetrievePersistedPostsAction;
  posts: IPost[];
}

export interface IChangeNewPostContentAction {
  type: typeof ChangeNewPostContentAction;
  content: string;
  mention?: string;
}

export interface IAddNewPostAction {
  type: typeof AddNewPostAction;
  post: IPost;
}

export type PostsActionTypes =
  | IRetrievePersistedPostsAction
  | IChangeNewPostContentAction
  | IAddNewPostAction;
