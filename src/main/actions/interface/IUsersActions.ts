import { IUser } from '../../model/IUser';

export const AttemptFetchUsersAction = 'AttemptFetchUsersAction';
export const SuccessFetchUsersAction = 'SuccessFetchUsersAction';
export const FailureFetchUsersAction = 'FailureFetchUsersAction';

export interface IAttemptFetchUsersAction {
  type: typeof AttemptFetchUsersAction;
}

export interface ISuccessFetchUsersAction {
  type: typeof SuccessFetchUsersAction;
  users: IUser[];
}

export interface IFailureFetchUsersAction {
  type: typeof FailureFetchUsersAction;
  error: string;
}

export type UsersActionTypes =
  | IAttemptFetchUsersAction
  | ISuccessFetchUsersAction
  | IFailureFetchUsersAction;
