import { IUser } from '../model/IUser';
import {
  AttemptFetchUsersAction,
  FailureFetchUsersAction,
  SuccessFetchUsersAction,
  UsersActionTypes,
} from './interface/IUsersActions';

export const AttemptFetchUsers = (): UsersActionTypes => ({
  type: AttemptFetchUsersAction,
});

export const SuccessFetchUsers = (users: IUser[]): UsersActionTypes => ({
  type: SuccessFetchUsersAction,
  users,
});

export const FailureFetchUsers = (error: string): UsersActionTypes => ({
  type: FailureFetchUsersAction,
  error,
});
