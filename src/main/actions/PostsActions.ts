import { IPost } from '../model/IPost';
import {
  AddNewPostAction,
  ChangeNewPostContentAction,
  PostsActionTypes,
  RetrievePersistedPostsAction,
} from './interface/IPostsActions';

export const RetrievePersistedPosts = (posts: IPost[]): PostsActionTypes => ({
  type: RetrievePersistedPostsAction,
  posts,
});

export const ChangeNewPostContent = (
  content: string,
  mention?: string
): PostsActionTypes => ({
  type: ChangeNewPostContentAction,
  content,
  mention,
});

export const AddNewPost = (post: IPost): PostsActionTypes => ({
  type: AddNewPostAction,
  post,
});
