import { ApiRequestStatus } from '../model/IApiRequest';
import { IAppState } from './interface/IAppState';

export const AppInitialState: IAppState = {
  postsState: {
    newPost: {
      content: '',
      mentions: [],
    },
    posts: [],
  },
  usersState: {
    request: {
      status: ApiRequestStatus.INITIAL,
      error: '',
    },
    users: [],
  },
};
