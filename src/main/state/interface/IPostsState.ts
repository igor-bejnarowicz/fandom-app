import { INewPost, IPost } from '../../model/IPost';

export interface IPostsState {
  newPost: INewPost;
  posts: IPost[];
}
