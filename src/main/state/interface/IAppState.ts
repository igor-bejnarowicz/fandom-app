import { IPostsState } from './IPostsState';
import { IUsersState } from './IUsersState';

export interface IAppState {
  postsState: IPostsState;
  usersState: IUsersState;
}
