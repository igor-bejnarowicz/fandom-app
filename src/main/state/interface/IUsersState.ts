import { IApiRequest } from '../../model/IApiRequest';
import { IUser } from '../../model/IUser';

export interface IUsersState {
  request: IApiRequest;
  users: IUser[];
}
