export enum ApiRequestStatus {
  INITIAL,
  PENDING,
  SUCCESS,
  FAILURE,
}

export interface IApiRequest {
  status: ApiRequestStatus;
  error?: string;
}
