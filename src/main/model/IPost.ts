export interface INewPost {
  content: string;
  mentions: string[];
}

export interface IPost extends INewPost {
  createdAt: number;
}
