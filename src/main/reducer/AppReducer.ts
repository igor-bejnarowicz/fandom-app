import {
  AddNewPostAction,
  ChangeNewPostContentAction,
  PostsActionTypes,
  RetrievePersistedPostsAction,
} from '../actions/interface/IPostsActions';
import {
  AttemptFetchUsersAction,
  FailureFetchUsersAction,
  SuccessFetchUsersAction,
  UsersActionTypes,
} from '../actions/interface/IUsersActions';
import { ApiRequestStatus } from '../model/IApiRequest';
import { IAppState } from '../state/interface/IAppState';

export type AppActionTypes = PostsActionTypes | UsersActionTypes;

export function AppReducer(
  state: IAppState,
  action: AppActionTypes
): IAppState {
  switch (action.type) {
    case RetrievePersistedPostsAction: {
      return {
        ...state,
        postsState: {
          ...state.postsState,
          posts: action.posts,
        },
      };
    }
    case ChangeNewPostContentAction: {
      const currentMentions = state.postsState.newPost.mentions;

      return {
        ...state,
        postsState: {
          ...state.postsState,
          newPost: {
            content: action.content,
            mentions: action.mention
              ? [...currentMentions, action.mention]
              : currentMentions,
          },
        },
      };
    }
    case AddNewPostAction: {
      return {
        ...state,
        postsState: {
          newPost: {
            content: '',
            mentions: [],
          },
          posts: [action.post, ...state.postsState.posts],
        },
      };
    }
    case AttemptFetchUsersAction: {
      return {
        ...state,
        usersState: {
          users: [],
          request: {
            status: ApiRequestStatus.PENDING,
            error: '',
          },
        },
      };
    }
    case SuccessFetchUsersAction: {
      return {
        ...state,
        usersState: {
          users: action.users,
          request: {
            status: ApiRequestStatus.SUCCESS,
            error: '',
          },
        },
      };
    }
    case FailureFetchUsersAction: {
      return {
        ...state,
        usersState: {
          ...state.usersState,
          request: {
            status: ApiRequestStatus.FAILURE,
            error: action.error,
          },
        },
      };
    }
    default: {
      return state;
    }
  }
}
