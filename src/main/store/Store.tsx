import React, { createContext, Dispatch, useReducer } from 'react';
import { AppActionTypes, AppReducer } from '../reducer/AppReducer';
import { AppInitialState } from '../state/AppInitialState';
import { IAppState } from '../state/interface/IAppState';

export const StoreContext = createContext<
  [IAppState, Dispatch<AppActionTypes>]
>([AppInitialState, (action: AppActionTypes) => ({ type: action.type })]);

const Store: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, AppInitialState);

  return (
    <StoreContext.Provider value={[state, dispatch]}>
      {children}
    </StoreContext.Provider>
  );
};

export default Store;
