import React from 'react';
import './Button.scss';

interface IProps {
  text: string;
  onClick: () => void;
}

const Button: React.FC<IProps> = ({ text, onClick }) => {
  return (
    <div className={'button'} onClick={onClick}>
      {text}
    </div>
  );
};

export default Button;
