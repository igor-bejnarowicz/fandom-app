import React from 'react';
import { ApiRequestStatus } from '../../../model/IApiRequest';
import { i18n } from '../../../utils/i18n';
import MentionElement from './mentionElement/MentionElement';
import './Mention.scss';

interface IProps {
  query: string | undefined;
  dataStatus: ApiRequestStatus;
  data: any[];
  onClick: (name: string) => void;
}

const Mention: React.FC<IProps> = ({ query, dataStatus, data, onClick }) => {
  if (dataStatus === ApiRequestStatus.PENDING) {
    return <div className={'mention mention--loading'}>{i18n.LOADING}</div>;
  }

  if (dataStatus === ApiRequestStatus.FAILURE) {
    return <div className={'mention mention--error'}>{i18n.ERROR}</div>;
  }

  if (dataStatus === ApiRequestStatus.SUCCESS && query && !data.length) {
    return <div className={'mention mention--no-users'}>{i18n.NO_USERS}</div>;
  }

  return query ? (
    <div className={'mention'}>
      {data.map((el, index) => (
        <MentionElement
          key={`mention-${el.name}-${index}`}
          text={el.name}
          onClick={() => onClick(el.name)}
        />
      ))}
    </div>
  ) : null;
};

export default Mention;
