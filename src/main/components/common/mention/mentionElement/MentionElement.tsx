import React from 'react';
import './MentionElement.scss';

interface IProps {
  text: string;
  onClick: () => void;
}

const MentionElement: React.FC<IProps> = ({ text, onClick }) => {
  return (
    <div className={'mention-element'} onClick={onClick}>
      {text}
    </div>
  );
};

export default MentionElement;
