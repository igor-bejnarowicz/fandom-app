import React from 'react';
import {
  useDebouncedFetchUsersEffect,
  usePostsState,
  useUsersState,
} from '../../../../utils/CustomHooks';
import Mention from '../../../common/mention/Mention';

export const MENTION_TRIGGER = '@';
const DEBOUNCE_DELAY_IN_MS = 500;

interface IProps {
  onClick: (userName: string) => void;
}

const UsersMention: React.FC<IProps> = ({ onClick }) => {
  const [postsState] = usePostsState();
  const [usersState] = useUsersState();
  const regex = new RegExp(`${MENTION_TRIGGER}(\\w+)$`);
  const userQuery = regex.exec(postsState.newPost.content)?.pop();

  useDebouncedFetchUsersEffect(userQuery, DEBOUNCE_DELAY_IN_MS);

  return (
    <Mention
      query={userQuery}
      dataStatus={usersState.request.status}
      data={usersState.users}
      onClick={onClick}
    />
  );
};

export default UsersMention;
