import React, { SyntheticEvent, useRef } from 'react';
import { ChangeNewPostContent } from '../../../actions/PostsActions';
import { usePostsState } from '../../../utils/CustomHooks';
import UsersMention, { MENTION_TRIGGER } from './usersMention/UsersMention';
import './NewPost.scss';

const NewPost: React.FC = () => {
  const [state, dispatch] = usePostsState();
  const textareaRef = useRef<any>({});

  const onContentChange = (e: SyntheticEvent<HTMLTextAreaElement>) => {
    dispatch(ChangeNewPostContent(e.currentTarget.value));
  };

  const onMentionClick = (userName: string) => {
    const lastTrigger = state.newPost.content.lastIndexOf(MENTION_TRIGGER);
    const newContent =
      state.newPost.content.slice(0, lastTrigger + 1) + userName + ' ';
    dispatch(ChangeNewPostContent(newContent, userName));
    textareaRef.current.focus();
  };

  return (
    <div className={'new-post'}>
      <textarea
        rows={3}
        className={'new-post new-post__content'}
        ref={textareaRef}
        onChange={onContentChange}
        value={state.newPost.content}
      />
      <UsersMention onClick={onMentionClick} />
    </div>
  );
};

export default NewPost;
