import React, { useEffect } from 'react';
import { RetrievePersistedPosts } from '../../../actions/PostsActions';
import { usePostsState } from '../../../utils/CustomHooks';
import PostsListEntry from './post/PostsListEntry';

const LOCALSTORAGE_POSTS_KEY = 'persistedPosts';

const PostsList: React.FC = () => {
  const [state, dispatch] = usePostsState();

  useEffect(() => {
    // try to retrieve localStorage posts once upon initial render
    const persistedPosts = localStorage.getItem(LOCALSTORAGE_POSTS_KEY);
    if (persistedPosts) {
      dispatch(RetrievePersistedPosts(JSON.parse(persistedPosts)));
    }
  }, []);

  useEffect(() => {
    // save current posts upon every posts list change
    localStorage.setItem(LOCALSTORAGE_POSTS_KEY, JSON.stringify(state.posts));
  }, [state.posts]);

  return (
    <>
      {state.posts.map((post, index) => (
        <PostsListEntry key={`post-${index}`} post={post} />
      ))}
    </>
  );
};

export default PostsList;
