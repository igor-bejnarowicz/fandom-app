import React from 'react';
import { IPost } from '../../../../model/IPost';
import './PostsListEntry.scss';
import { getFormattedPostContent } from '../../../../utils/TextUtils';

interface IProps {
  post: IPost;
}

const PostsListEntry: React.FC<IProps> = ({ post }) => {
  return (
    <div className={'posts-list-entry'}>
      <div>{getFormattedPostContent(post)}</div>
      <div className={'posts-list-entry__date'}>
        {new Date(post.createdAt).toLocaleString()}
      </div>
    </div>
  );
};

export default PostsListEntry;
