import React from 'react';
import { AddNewPost } from '../../../actions/PostsActions';
import { usePostsState } from '../../../utils/CustomHooks';
import { i18n } from '../../../utils/i18n';
import Button from '../../common/button/Button';
import './AddPostButton.scss';

const AddPostButton: React.FC = () => {
  const [state, dispatch] = usePostsState();

  const onClick = () => {
    if (state.newPost.content) {
      dispatch(
        AddNewPost({
          content: state.newPost.content,
          mentions: state.newPost.mentions,
          createdAt: new Date().getTime(),
        })
      );
    }
  };

  return (
    <div className={'add-post-button'}>
      <Button text={i18n.ADD} onClick={onClick} />
    </div>
  );
};

export default AddPostButton;
