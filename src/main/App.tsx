import React from 'react';
import AddPostButton from './components/posts/addPostButton/AddPostButton';
import NewPost from './components/posts/newPost/NewPost';
import PostsList from './components/posts/postsList/PostsList';
import Store from './store/Store';
import { i18n } from './utils/i18n';
import './App.scss';
import logo from './styles/logo.svg';

function App() {
  return (
    <Store>
      <div className="app">
        <a className={'app__logo-link'} href={'https://www.fandom.com/'}>
          <img className={'app__logo'} src={logo} alt={'logo'} />
        </a>
        <header className="app__header">{i18n.DESCRIPTION}</header>
        <NewPost />
        <AddPostButton />
        <PostsList />
      </div>
    </Store>
  );
}

export default App;
