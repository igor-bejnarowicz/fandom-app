// Using proxy to deal with CORS
const baseUrl: string =
  'https://cors-anywhere.herokuapp.com/https://community.fandom.com/api.php';

export const usersRequestParams = (query: string): URLSearchParams =>
  new URLSearchParams({
    format: 'json',
    action: 'query',
    list: 'allusers',
    auprefix: query,
  });

export async function fetchUsersFromApi(query: string): Promise<Response> {
  return await fetch(`${baseUrl}?${usersRequestParams(query)}`, {
    method: 'GET',
  });
}
