export enum i18n {
  ADD = 'Add',
  DESCRIPTION = 'Welcome. What you can see below is a React application written in the recruitment process for FANDOM.',
  ERROR = 'Error retrieving data.',
  LOADING = 'Loading...',
  NO_USERS = 'No users found.',
}
