import React from 'react';
import { IPost } from '../model/IPost';

// Although the task specified usernames without whitespace characters, most
// of the names fetched from the API do in fact contain spaces.
// Whitespace usernames are more tricky to detect and to highlight properly,
// but since I decided to handle that case nonetheless.
// Hence: the following approach, which handles usernames with whitespaces correctly
export function getFormattedPostContent(post: IPost) {
  const mentionsPositionsInPost = post.mentions.map(mention => ({
    // '- 1' to also include '@' sign as part of the @mention tag
    start: post.content.indexOf(mention) - 1,
    end: post.content.indexOf(mention) + mention.length,
  }));

  const result: JSX.Element[] = [];

  const pushTextToResult = (
    startPos: number,
    endPos: number,
    className?: string
  ) => {
    result.push(
      <span
        key={`post-${post.createdAt}-text-${startPos}-${endPos - 1}`}
        className={className}
      >
        {post.content.substring(startPos, endPos)}
      </span>
    );
  };

  const pushRegularText = (startPos: number, endPos: number) => {
    pushTextToResult(startPos, endPos);
  };

  const pushDecoratedText = (startPos: number, endPos: number) => {
    pushTextToResult(startPos, endPos, 'posts-list-entry__mention');
  };

  const endPositionOfMentionsInPost = mentionsPositionsInPost.reduce(
    (currentPosition, mentionPos) => {
      // create regular text in-between mentions
      pushRegularText(currentPosition, mentionPos.start);
      // create styled text for every mention
      pushDecoratedText(mentionPos.start, mentionPos.end);

      return mentionPos.end;
    },
    0
  );

  // push one final regular text for either:
  // 1. a post without mentions
  // 2. final piece of text between the last mention and the end of the post
  pushRegularText(endPositionOfMentionsInPost, post.content.length);
  return result;
}
