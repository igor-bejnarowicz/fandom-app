import { Dispatch, useCallback, useContext, useEffect, useRef } from 'react';
import {
  AttemptFetchUsers,
  FailureFetchUsers,
  SuccessFetchUsers,
} from '../actions/UsersActions';
import { AppActionTypes } from '../reducer/AppReducer';
import { IPostsState } from '../state/interface/IPostsState';
import { IUsersState } from '../state/interface/IUsersState';
import { StoreContext } from '../store/Store';
import { fetchUsersFromApi } from './ApiUtils';

export const usePostsState = (): [IPostsState, Dispatch<AppActionTypes>] => {
  const [state, dispatch] = useContext(StoreContext);
  return [state.postsState, dispatch];
};

export const useUsersState = (): [IUsersState, Dispatch<AppActionTypes>] => {
  const [state, dispatch] = useContext(StoreContext);
  return [state.usersState, dispatch];
};

export const useDispatch = (): Dispatch<AppActionTypes> => {
  return useContext(StoreContext)[1];
};

export const useDebouncedFetchUsersEffect = (
  userQuery: string | undefined,
  delayInMs: number
): void => {
  const dispatch = useDispatch();
  const fetchUsersDebounced = useDebouncer(async (query: string) => {
    try {
      const result = await fetchUsersFromApi(query);
      const jsonResult = await result.json();
      dispatch(SuccessFetchUsers(jsonResult.query.allusers));
    } catch (e) {
      dispatch(FailureFetchUsers((e as Error).message));
    }
  }, delayInMs);

  useEffect(() => {
    if (userQuery) {
      dispatch(AttemptFetchUsers());
      fetchUsersDebounced(userQuery);
    }
    // according to https://reactjs.org/docs/hooks-reference.html#usereducer
    // it's safe to omit dispatch from the deps list
    /* eslint-disable-next-line */
  }, [userQuery]);
};

export function useDebouncer<T extends any[]>(
  fnToDebounce: (...args: T) => any,
  delayInMs: number
): (...args: T) => void {
  const ref = useRef<any>(undefined);
  const debouncer = useCallback(
    (...args: T): void => {
      if (ref.current) {
        clearTimeout(ref.current);
      }

      ref.current = setTimeout(() => {
        ref.current = undefined;
        fnToDebounce(...args);
      }, delayInMs);
    },
    [fnToDebounce, delayInMs]
  );

  return debouncer;
}
